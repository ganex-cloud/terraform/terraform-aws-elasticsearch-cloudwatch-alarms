locals {
  alarm_name_prefix = "[${title(var.alarm_name_prefix == "" ? data.aws_iam_account_alias.current.account_alias : var.alarm_name_prefix)}]"
  thresholds = {
    FreeStorageSpaceThreshold        = max(var.free_storage_space_threshold, 0)
    MinimumAvailableNodes            = max(var.min_available_nodes, 0)
    CPUUtilizationThreshold          = min(max(var.cpu_utilization_threshold, 0), 100)
    JVMMemoryPressureThreshold       = min(max(var.jvm_memory_pressure_threshold, 0), 100)
    MasterCPUUtilizationThreshold    = min(max(coalesce(var.master_cpu_utilization_threshold, var.cpu_utilization_threshold), 0), 100)
    MasterJVMMemoryPressureThreshold = min(max(coalesce(var.master_jvm_memory_pressure_threshold, var.jvm_memory_pressure_threshold), 0), 100)
    KibanaHealthyNodes               = max(var.min_kibana_healthy_nodes, 0)
    DiskQueueDepth                   = max(var.max_disk_queue_depth, 100)
    ThreadpoolWriteQueue             = max(var.max_thread_pool_write_queue, 100)
    ThreadpoolSearchQueue            = max(var.max_thread_pool_search_queue, 500)
    PrimaryWriteRejected             = max(var.max_primary_write_rejected, 1)
    ReplicaWriteRejected             = max(var.max_replica_write_rejected, 1)
  }
}

# Alarms
resource "aws_cloudwatch_metric_alarm" "cluster_status_is_red" {
  count               = var.monitor_cluster_status_is_red ? 1 : 0
  alarm_name          = "${title(local.alarm_name_prefix)} es-${var.domain_name}-ClusterStatusIsRed${var.alarm_name_postfix}. (${var.cluster_status_is_red-priority})"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "ClusterStatus.red"
  namespace           = "AWS/ES"
  period              = "60"
  statistic           = "Maximum"
  threshold           = "1"
  alarm_description   = "Average elasticsearch cluster status is in red over last 1 minutes"
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags
  treat_missing_data  = "ignore"

  dimensions = {
    DomainName = var.domain_name
    ClientId   = data.aws_caller_identity.default.account_id
  }
}

resource "aws_cloudwatch_metric_alarm" "cluster_status_is_yellow" {
  count               = var.monitor_cluster_status_is_yellow ? 1 : 0
  alarm_name          = "${title(local.alarm_name_prefix)} es-${var.domain_name}-ClusterStatusIsYellow${var.alarm_name_postfix}. (${var.cluster_status_is_yellow-priority})"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "ClusterStatus.yellow"
  namespace           = "AWS/ES"
  period              = "60"
  statistic           = "Maximum"
  threshold           = "1"
  alarm_description   = "Average elasticsearch cluster status is in yellow over last 1 minutes"
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags
  treat_missing_data  = "ignore"

  dimensions = {
    DomainName = var.domain_name
    ClientId   = data.aws_caller_identity.default.account_id
  }
}

resource "aws_cloudwatch_metric_alarm" "free_storage_space_too_low" {
  count               = var.monitor_free_storage_space_too_low ? 1 : 0
  alarm_name          = "${title(local.alarm_name_prefix)} es-${var.domain_name}-FreeStorageSpaceTooLow${var.alarm_name_postfix}. (${var.free_storage_space_too_low-priority})"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "FreeStorageSpace"
  namespace           = "AWS/ES"
  period              = "60"
  statistic           = "Minimum"
  threshold           = local.thresholds["FreeStorageSpaceThreshold"]
  alarm_description   = "Average elasticsearch free storage space over last 1 minutes is too low"
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags
  treat_missing_data  = "ignore"

  dimensions = {
    DomainName = var.domain_name
    ClientId   = data.aws_caller_identity.default.account_id
  }
}

resource "aws_cloudwatch_metric_alarm" "cluster_index_writes_blocked" {
  count               = var.monitor_cluster_index_writes_blocked ? 1 : 0
  alarm_name          = "${title(local.alarm_name_prefix)} es-${var.domain_name}-ClusterIndexWritesBlocked${var.alarm_name_postfix}. (${var.cluster_index_writes_blocked-priority})"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "ClusterIndexWritesBlocked"
  namespace           = "AWS/ES"
  period              = "300"
  statistic           = "Maximum"
  threshold           = "1"
  alarm_description   = "Elasticsearch index writes being blocker over last 5 minutes"
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags
  treat_missing_data  = "ignore"

  dimensions = {
    DomainName = var.domain_name
    ClientId   = data.aws_caller_identity.default.account_id
  }
}

resource "aws_cloudwatch_metric_alarm" "insufficient_available_nodes" {
  count               = var.monitor_insufficient_available_nodes ? 1 : 0
  alarm_name          = "${title(local.alarm_name_prefix)} es-${var.domain_name}-InsufficientAvailableNodes${var.alarm_name_postfix}. (${var.insufficient_available_nodes-priority})"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "Nodes"
  namespace           = "AWS/ES"
  period              = "86400"
  statistic           = "Average"
  threshold           = local.thresholds["MinimumAvailableNodes"]
  alarm_description   = "Elasticsearch nodes minimum < ${local.thresholds["MinimumAvailableNodes"]} for 1 day"
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags
  treat_missing_data  = "ignore"

  dimensions = {
    DomainName = var.domain_name
    ClientId   = data.aws_caller_identity.default.account_id
  }
}

resource "aws_cloudwatch_metric_alarm" "automated_snapshot_failure" {
  count               = var.monitor_automated_snapshot_failure ? 1 : 0
  alarm_name          = "${title(local.alarm_name_prefix)} es-${var.domain_name}-AutomatedSnapshotFailure${var.alarm_name_postfix}. (${var.automated_snapshot_failure-priority})"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "AutomatedSnapshotFailure"
  namespace           = "AWS/ES"
  period              = "60"
  statistic           = "Maximum"
  threshold           = "1"
  alarm_description   = "Elasticsearch automated snapshot failed over last 1 minutes"
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags
  treat_missing_data  = "ignore"

  dimensions = {
    DomainName = var.domain_name
    ClientId   = data.aws_caller_identity.default.account_id
  }
}

resource "aws_cloudwatch_metric_alarm" "cpu_utilization_too_high" {
  count               = var.monitor_cpu_utilization_too_high ? 1 : 0
  alarm_name          = "${title(local.alarm_name_prefix)} es-${var.domain_name}-CPUUtilizationTooHigh${var.alarm_name_postfix}. (${var.cpu_utilization_too_high-priority})"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ES"
  period              = "900"
  statistic           = "Average"
  threshold           = local.thresholds["CPUUtilizationThreshold"]
  alarm_description   = "Average elasticsearch cluster CPU utilization over last 45 minutes too high"
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags

  dimensions = {
    DomainName = var.domain_name
    ClientId   = data.aws_caller_identity.default.account_id
  }
}

resource "aws_cloudwatch_metric_alarm" "jvm_memory_pressure_too_high" {
  count               = var.monitor_jvm_memory_pressure_too_high ? 1 : 0
  alarm_name          = "${title(local.alarm_name_prefix)} es-${var.domain_name}-JVMMemoryPressure${var.alarm_name_postfix}. (${var.jvm_memory_pressure_too_high-priority})"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "JVMMemoryPressure"
  namespace           = "AWS/ES"
  period              = "60"
  statistic           = "Maximum"
  threshold           = local.thresholds["JVMMemoryPressureThreshold"]
  alarm_description   = "Elasticsearch JVM memory pressure is too high over last 3 minutes"
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags

  dimensions = {
    DomainName = var.domain_name
    ClientId   = data.aws_caller_identity.default.account_id
  }
}

resource "aws_cloudwatch_metric_alarm" "master_cpu_utilization_too_high" {
  count               = var.monitor_master_cpu_utilization_too_high ? 1 : 0
  alarm_name          = "${title(local.alarm_name_prefix)} es-${var.domain_name}-MasterCPUUtilizationTooHigh${var.alarm_name_postfix}. (${var.master_cpu_utilization_too_high-priority})"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "MasterCPUUtilization"
  namespace           = "AWS/ES"
  period              = "900"
  statistic           = "Average"
  threshold           = local.thresholds["MasterCPUUtilizationThreshold"]
  alarm_description   = "Average elasticsearch cluster CPU utilization over last 45 minutes too high"
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags

  dimensions = {
    DomainName = var.domain_name
    ClientId   = data.aws_caller_identity.default.account_id
  }
}

resource "aws_cloudwatch_metric_alarm" "master_jvm_memory_pressure_too_high" {
  count               = var.monitor_master_jvm_memory_pressure_too_high ? 1 : 0
  alarm_name          = "${title(local.alarm_name_prefix)} es-${var.domain_name}-JVMMemoryPressure${var.alarm_name_postfix}. (${var.master_jvm_memory_pressure_too_high-priority})"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "MasterJVMMemoryPressure"
  namespace           = "AWS/ES"
  period              = "300"
  statistic           = "Maximum"
  threshold           = local.thresholds["MasterJVMMemoryPressureThreshold"]
  alarm_description   = "Elasticsearch JVM memory pressure is too high over last 15 minutes"
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags

  dimensions = {
    DomainName = var.domain_name
    ClientId   = data.aws_caller_identity.default.account_id
  }
}

resource "aws_cloudwatch_metric_alarm" "kibana_healthy_nodes" {
  count               = var.monitor_kibana_healthy_nodes ? 1 : 0
  alarm_name          = "${title(local.alarm_name_prefix)} es-${"${var.domain_name}"}-KibanaHealthyNodes${var.alarm_name_postfix}. (${var.kibana_healthy_nodes-priority})"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "KibanaHealthyNodes"
  namespace           = "AWS/ES"
  period              = "300"
  statistic           = "Average"
  threshold           = local.thresholds["KibanaHealthyNodes"]
  alarm_description   = "Kibana is unhealthy"
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags

  dimensions = {
    DomainName = var.domain_name
    ClientId   = data.aws_caller_identity.default.account_id
  }
}

resource "aws_cloudwatch_metric_alarm" "disk_queue_depth_too_high" {
  count               = var.monitor_disk_queue_depth ? 1 : 0
  alarm_name          = "${title(local.alarm_name_prefix)} es-${"${var.domain_name}"}-DiskQueueDepth${var.alarm_name_postfix}. (${var.disk_queue_depth_too_high-priority})"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "DiskQueueDepth"
  namespace           = "AWS/ES"
  period              = "300"
  statistic           = "Average"
  threshold           = local.thresholds["DiskQueueDepth"]
  alarm_description   = "Disk Queue Depth is the number of I/O requests that are queued at a time against the storage. This could indicate a surge in requests or Amazon EBS throttling, resulting in increased latency."
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags

  dimensions = {
    DomainName = var.domain_name
    ClientId   = data.aws_caller_identity.default.account_id
  }
}


resource "aws_cloudwatch_metric_alarm" "thread_pool_write_queue" {
  count               = var.monitor_thread_pool_write_queue ? 1 : 0
  alarm_name          = "${title(local.alarm_name_prefix)} es-${"${var.domain_name}"}-ThreadpoolWriteQueue${var.alarm_name_postfix}. (${var.thread_pool_write_queue-priority})"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "ThreadpoolWriteQueue"
  namespace           = "AWS/ES"
  period              = "60"
  statistic           = "Average"
  threshold           = local.thresholds["ThreadpoolWriteQueue"]
  alarm_description   = "The cluster is experiencing high indexing concurrency. Review and control indexing requests, or increase cluster resources."
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags

  dimensions = {
    DomainName = var.domain_name
    ClientId   = data.aws_caller_identity.default.account_id
  }
}

resource "aws_cloudwatch_metric_alarm" "thread_pool_search_queue" {
  count               = var.monitor_thread_pool_search_queue ? 1 : 0
  alarm_name          = "${title(local.alarm_name_prefix)} es-${"${var.domain_name}"}-ThreadpoolSearchQueue${var.alarm_name_postfix}. (${var.thread_pool_search_queue-priority})"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "ThreadpoolSearchQueue"
  namespace           = "AWS/ES"
  period              = "60"
  statistic           = "Maximum"
  threshold           = local.thresholds["ThreadpoolSearchQueue"]
  alarm_description   = "The cluster is experiencing high search concurrency. Consider scaling your cluster. You can also increase the search queue size, but increasing it excessively can cause out of memory errors."
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags

  dimensions = {
    DomainName = var.domain_name
    ClientId   = data.aws_caller_identity.default.account_id
  }
}

resource "aws_cloudwatch_metric_alarm" "primary_write_rejected" {
  count               = var.monitor_primary_write_rejected ? 1 : 0
  alarm_name          = "${title(local.alarm_name_prefix)} es-${"${var.domain_name}"}-PrimaryWriteRejected${var.alarm_name_postfix}. (${var.primary_write_rejected-priority})"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "PrimaryWriteRejected"
  namespace           = "AWS/ES"
  period              = "60"
  statistic           = "Maximum"
  threshold           = local.thresholds["PrimaryWriteRejected"]
  alarm_description   = "The total number of rejections happened on the primary shards due to indexing pressure since the last OpenSearch Service process startup."
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags

  dimensions = {
    DomainName = var.domain_name
    ClientId   = data.aws_caller_identity.default.account_id
  }
}

resource "aws_cloudwatch_metric_alarm" "replica-write-rejected" {
  count               = var.monitor_replica_write_rejected ? 1 : 0
  alarm_name          = "${title(local.alarm_name_prefix)} es-${"${var.domain_name}"}-ReplicaWriteRejected${var.alarm_name_postfix}. (${var.replica-write-rejected-priority})"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "ReplicaWriteRejected"
  namespace           = "AWS/ES"
  period              = "60"
  statistic           = "Maximum"
  threshold           = local.thresholds["ReplicaWriteRejected"]
  alarm_description   = "The total number of rejections happened on the replica shards due to indexing pressure since the last OpenSearch Service process startup."
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags

  dimensions = {
    DomainName = var.domain_name
    ClientId   = data.aws_caller_identity.default.account_id
  }
}
