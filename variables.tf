variable "domain_name" {
  description = "The Elasticserach domain name you want to monitor."
  type        = string
}

variable "sns_topic_arn" {
  description = "A list of ARNs (i.e. SNS Topic ARN) to notify on alerts"
  type        = list(string)
}

variable "alarm_name_prefix" {
  description = "Alarm name prefix"
  type        = string
  default     = ""
}

variable "alarm_name_postfix" {
  description = "Alarm name postfix"
  type        = string
  default     = ""
}

variable "monitor_cluster_status_is_red" {
  description = "Enable monitoring of cluster status is in red"
  type        = bool
  default     = true
}

variable "monitor_cluster_status_is_yellow" {
  description = "Enable monitoring of cluster status is in yellow"
  type        = bool
  default     = true
}

variable "monitor_free_storage_space_too_low" {
  description = "Enable monitoring of cluster average free storage is to low"
  type        = bool
  default     = true
}

variable "monitor_cluster_index_writes_blocked" {
  description = "Enable monitoring of cluster index writes being blocked"
  type        = bool
  default     = true
}

variable "monitor_insufficient_available_nodes" {
  description = "Enable monitoring insufficient available nodes"
  type        = bool
  default     = false
}

variable "monitor_automated_snapshot_failure" {
  description = "Enable monitoring of automated snapshot failure"
  type        = bool
  default     = true
}

variable "monitor_cpu_utilization_too_high" {
  description = "Enable monitoring of CPU utilization is too high"
  type        = bool
  default     = true
}

variable "monitor_jvm_memory_pressure_too_high" {
  description = "Enable monitoring of JVM memory pressure is too high"
  type        = bool
  default     = true
}

variable "monitor_kibana_healthy_nodes" {
  description = "Enable monitoring of Kibana Healthy Nodes"
  default     = true
}

variable "monitor_disk_queue_depth" {
  description = "Enable monitoring of Disk Queue Depth"
  default     = true
}

variable "monitor_thread_pool_search_queue" {
  description = "Enable monitoring of ThreadpoolSearchQueue"
  default     = true
}

variable "monitor_thread_pool_write_queue" {
  description = "Enable monitoring of ThreadpoolWriteQueue"
  default     = true
}

variable "monitor_primary_write_rejected" {
  description = "Enable monitoring of PrimaryWriteRejected"
  default     = true
}

variable "monitor_replica_write_rejected" {
  description = "Enable monitoring of ReplicaWriteRejected"
  default     = true
}

variable "monitor_master_cpu_utilization_too_high" {
  description = "Enable monitoring of CPU utilization of master nodes are too high. Only enable this when dedicated master is enabled"
  type        = bool
  default     = false
}

variable "monitor_master_jvm_memory_pressure_too_high" {
  description = "Enable monitoring of JVM memory pressure of master nodes are too high. Only enable this wwhen dedicated master is enabled"
  type        = bool
  default     = false
}

variable "free_storage_space_threshold" {
  description = "The minimum amount of available storage space in MegaByte."
  type        = number
  default     = 20480 ## 20 Gigabyte in MegaByte
}

variable "min_kibana_healthy_nodes" {
  description = "The minimum available (reachable) kibana nodes to have"
  default     = 1
}

variable "min_available_nodes" {
  description = "The minimum available (reachable) nodes to have"
  type        = number
  default     = 1
}

variable "cpu_utilization_threshold" {
  description = "The maximum percentage of CPU utilization"
  type        = number
  default     = 80 # 80 percent in Percentage
}

variable "jvm_memory_pressure_threshold" {
  description = "The maximum percentage of the Java heap used for all data nodes in the cluster"
  type        = number
  default     = 95 # 95 percent in Percentage
}

variable "master_cpu_utilization_threshold" {
  description = "The maximum percentage of CPU utilization of master nodes"
  type        = number
  default     = 50 # default same as `cpu_utilization_threshold` in Percentage
}

variable "master_jvm_memory_pressure_threshold" {
  description = "The maximum percentage of the Java heap used for master nodes in the cluster"
  type        = number
  default     = 95 # default same as `jvm_memory_pressure_threshold` in Percentage
}

variable "max_disk_queue_depth" {
  description = "The maximum max_disk_queue_depth"
  default     = 100
}

variable "max_thread_pool_write_queue" {
  description = "The max_thread_pool_write_queue"
  default     = 100
}

variable "max_thread_pool_search_queue" {
  description = "The thread_pool_search_queue"
  default     = 100
}

variable "max_primary_write_rejected" {
  description = "The max_primary_write_rejected"
  default     = 1
}

variable "max_replica_write_rejected" {
  description = "The max_replica_write_rejected"
  default     = 1
}

variable "tags" {
  description = "(Optional) A map of tags to assign to the all resources"
  type        = map(string)
  default     = {}
}

variable "cluster_status_is_red-priority" {
  description = "Priority of alarm"
  default     = "P1"
  type        = string
}

variable "cluster_status_is_yellow-priority" {
  description = "Priority of alarm"
  default     = "P3"
  type        = string
}

variable "free_storage_space_too_low-priority" {
  description = "Priority of alarm"
  default     = "P3"
  type        = string
}

variable "cluster_index_writes_blocked-priority" {
  description = "Priority of alarm"
  default     = "P2"
  type        = string
}

variable "insufficient_available_nodes-priority" {
  description = "Priority of alarm"
  default     = "P2"
  type        = string
}

variable "automated_snapshot_failure-priority" {
  description = "Priority of alarm"
  default     = "P4"
  type        = string
}

variable "cpu_utilization_too_high-priority" {
  description = "Priority of alarm"
  default     = "P3"
  type        = string
}

variable "jvm_memory_pressure_too_high-priority" {
  description = "Priority of alarm"
  default     = "P3"
  type        = string
}

variable "master_cpu_utilization_too_high-priority" {
  description = "Priority of alarm"
  default     = "P3"
  type        = string
}

variable "master_jvm_memory_pressure_too_high-priority" {
  description = "Priority of alarm"
  default     = "P3"
  type        = string
}

variable "kibana_healthy_nodes-priority" {
  description = "Priority of alarm"
  default     = "P2"
  type        = string
}

variable "disk_queue_depth_too_high-priority" {
  description = "Priority of alarm"
  default     = "P3"
  type        = string
}

variable "thread_pool_write_queue-priority" {
  description = "Priority of alarm"
  default     = "P3"
  type        = string
}

variable "thread_pool_search_queue-priority" {
  description = "Priority of alarm"
  default     = "P3"
  type        = string
}

variable "primary_write_rejected-priority" {
  description = "Priority of alarm"
  default     = "P2"
  type        = string
}

variable "replica-write-rejected-priority" {
  description = "Priority of alarm"
  default     = "P2"
  type        = string
}
